import { useState, useEffect } from 'react';

import PokemonList from '../PokemonList/PokemonList';
import styles from './Main.module.css';
import { getPokemons } from './../../service/pokemon.service';
import { Overlay } from '../Overlay/Overlay';

export const Main = () => {

    const [pokemons, setPokemons] = useState([]);
    const [pokemonInfo,setPokemonInfo] = useState([]);
    const[isModal,setModal] = useState(false);

    const toggle = (e,info = []) =>{
        e.preventDefault();
        console.log("Info",info)
        const temp = !isModal;
        setModal(temp);
        setPokemonInfo(info);
    }

    const getPokemonData = () => {
        const data = getPokemons();
        setPokemons(data);
    }

    const onClickHandler = (name) => {
        const pokemonClone = [...pokemons];
        const index = pokemonClone.findIndex(p => p.name === name);

        pokemonClone[index].isSeen = !pokemonClone[index].isSeen; 
        
        setPokemons(pokemonClone);

    }

    // when main is loaded/rendered to the screen for the first time
    // get the pokemon list
    useEffect(() => {
        getPokemonData();
    }, []);
    // when the dependency list is empty, this useEffect is going be called just 1 time
    // when the component is rendered.

    const seenPokemon = pokemons.filter(p => p.isSeen);
    const unSeenPokemon = pokemons.filter(p => !p.isSeen);

    return (
        <main className={styles.main}>
            <PokemonList 
                pokemons={seenPokemon}
                onClickHandler={onClickHandler}
                alertHandler={toggle}
            />

            <PokemonList 
                pokemons={unSeenPokemon}
                onClickHandler={onClickHandler}
                alertHandler={toggle}
            />

            {isModal && <Overlay data={pokemonInfo} clickHandler={toggle}/>}
        </main>
    )
}