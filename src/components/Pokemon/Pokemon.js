import styles from './Pokemon.module.css';

const Pokemon = ({ pokemon, onClickHandler ,alertHandler}) => {
    return (
        <div className={styles.pokemon}>
            <span onClick={onClickHandler}>{pokemon.name}</span>
            <button onClick={e => alertHandler(e,pokemon)}>+</button>
        </div>
    )
}

export default Pokemon;