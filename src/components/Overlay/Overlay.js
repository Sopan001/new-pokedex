import { useState } from "react";
import styles from "./Overlay.module.css";

export const Overlay = ({data,clickHandler}) => {
    console.log(data)
    return (
        <div className={styles.overlay} onClick={clickHandler}>
            <div className={styles.container}>
                <h2>{data.name}</h2>
                <h4>{data.info}</h4>
            </div>
        </div>
    );
}