import styles from './PokemonList.module.css';
import Pokemon from './../Pokemon/Pokemon';

const PokemonList = ({ pokemons, onClickHandler,alertHandler }) => {
    return (
        <div className={styles["pokemon-list"]}>
            {
                pokemons.map((pokemon) => {
                    return <Pokemon 
                        pokemon={pokemon} 
                        onClickHandler={() => {
                            onClickHandler(pokemon.name);
                        }}
                        alertHandler={alertHandler}
                    />;
                })
            }
        </div>
    )
};

export default PokemonList;