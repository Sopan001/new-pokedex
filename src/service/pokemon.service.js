// let's assume that this list comes from the API.
const pokemons = [
  {
    name: "Togepi",
    info: "The shell seems to be filled with joy. It is said that it will share good luck when treated kindly.",
    isSeen: false,
  },
  {
    name: "Charizard",
    info: "It spits fire that is hot enough to melt boulders. It may cause forest fires by blowing flames.",
    isSeen: false,
  },
  {
    name: "Pikachu",
    info:"Pikachu that can generate powerful electricity have cheek sacs that are extra soft and super stretchy.",
    isSeen: false,
  },
  {
    name: "Charmander",
    info:"It has a preference for hot things. When it rains, steam is said to spout from the tip of its tail.",
    isSeen: false,
  },
  {
    name: "Bulbasaur",
    info:"There is a plant seed on its back right from the day this Pokémon is born. The seed slowly grows larger.",
    isSeen: true,
  },
  {
    name: "Squirtle",
    info:"When it retracts its long neck into its shell, it squirts out water with vigorous force.",
    isSeen: false,
  },
];

export const getPokemons = () => pokemons;
